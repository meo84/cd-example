require 'webrick'
require_relative 'tiger'

server = WEBrick::HTTPServer.new :Port => 5000

server.mount_proc '/' do |request, response|
  response.body = Tiger.new.roar
end

server.start
